# -*- coding: utf-8 -*-

import sys
import io
import nltk
import ssl

ssl._create_default_https_context = ssl._create_unverified_context
nltk.download('punkt')

# operadores: +, -, *, /
# numeros: 0 - 100

# grammar definition
grammar = grammar = """
S ->  N | '(' S ')' | S O S
N -> '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '10' | '11' | '12' | '13' | '14' | '15' | '16' | '17' | '18' | '19' | '20' | '21' | '22' | '23' | '24' | '25' | '26' | '27' | '28' | '29' | '30' | '31' | '32' | '33' | '34' | '35' | '36' | '37' | '38' | '39' | '40' | '41' | '42' | '43' | '44' | '45' | '46' | '47' | '48' | '49' | '50' | '51' | '52' | '53' | '54' | '55' | '56' | '57' | '58' | '59' | '60' | '61' | '62' | '63' | '64' | '65' | '66' | '67' | '68' | '69' | '70' | '71' | '72' | '73' | '74' | '75' | '76' | '77' | '78' | '79' | '80' | '81' | '82' | '83' | '84' | '85' | '86' | '87' | '88' | '89' | '90' | '91' | '92' | '93' | '94' | '95' | '96' | '97' | '98' | '99' | '100'
O -> '+' | '-' | '*' | '/'
"""

def parse(s, grammar):
    grammar = nltk.CFG.fromstring(grammar)
    parser = nltk.LeftCornerChartParser(grammar)
    s_tokenized = nltk.word_tokenize(s)
    tree = list(parser.parse(s_tokenized))[:1]
    # imprimir en consola
    '''
    for tre in tree:
        tre.pretty_print()
    '''
    return tree

def calcular(tree):
    #print("Inicio recursion")
    if isinstance(tree, nltk.Tree):
        if tree.label() == 'S':
            #print("Caso S")
            if len(tree) == 1:  
                #print("Caso largo 1")
                return calcular(tree[0])
            elif len(tree) == 3:  # Caso de SOS o (S)
                #print("Caso largo 3")
                izq = tree[0]
                med = tree[1]
                der = tree[2]
                '''
                if (type(izq) is nltk.Tree):
                    print (izq.label())
                else:
                    print (izq)
                if (type(med) is nltk.Tree):
                    print (med.label())
                else:
                    print (med)
                if (type(der) is nltk.Tree):
                    print (der.label())
                else:
                    print (der)
                '''
                if (izq is not nltk.Tree and izq == '('):
                    #print("Caso ( S )")
                    return calcular(med)
                else : # Caso S O S
                    #print("Caso SOS")
                    left = calcular(izq) 
                    operator = calcular(med)
                    right = calcular(der)
                    #print(f"left: {left}, operator: {operator}, right: {right}") 
                    return f"{operator}({left},{right})"
        else:
            return ' '.join(tree.leaves())
    else:
        return tree


if __name__ == '__main__':
    archivo_entrada = sys.argv[1]
    archivo_salida = sys.argv[2]
    f = io.open(archivo_entrada, 'r', newline='\n', encoding='utf-8')
    s = f.read().strip()
    f.close()

    try:
        tree = parse(s, grammar)
        if tree:
            salida = calcular(tree[0])
        else:
            salida = "NO PERTENECE"
    except ValueError:
        salida = "NO PERTENECE - FUERA DE ALFABETO"
    except Exception as e:
        salida = "NO PERTENECE - FUERA DE ALFABETO"

    with io.open(archivo_salida, 'w', newline='\n', encoding='utf-8') as f:
        f.write(salida)