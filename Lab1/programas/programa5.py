# -*- coding: utf-8 -*-
import re
import sys

def prog(text):
    # Patrones de expresiones regulares para las conversiones
    patterns = [
	(r'###+([^\\]+)', r'<h3>\1</h3>'),
	(r'##+([^\\]+)', r'<h2>\1</h2>'),
        (r'#+([^\\]+)', r'<h1>\1</h1>'),    
        (r'\*\*+([^\\]+)\*\*', r'<strong>\1</strong>'),
        (r'\*+([^\\]+)\*', r'<em>\1</em>'),
        (r'~~+([^\\]+)~~', r'<s>\1</s>')
    ]

    # Aplicar las conversiones
    html_text = text
    for pattern, replacement in patterns:
        html_text = re.sub(pattern, replacement, html_text)
    
    return html_text


if __name__ == '__main__':
    entrada = sys.argv[1]  # archivo entrada (param)
    salida = sys.argv[2]   # archivo salida (param)
    
    f = open(entrada, 'r') # abrir archivo entrada
    datos = f.read()       # leer archivo entrada
    f.close()              # cerrar archivo entrada
    
    ret = prog(datos)      # ejecutar er
    
    f = open(salida, 'w')  # abrir archivo salida
    f.write(ret)           # escribir archivo salida
    f.close()              # cerrar archivo salida


