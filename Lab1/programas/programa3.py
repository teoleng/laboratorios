# -*- coding: utf-8 -*-
import re
import sys

def format_timestamp(timestamp):
    parts = re.split(r'[T:\s]', timestamp)
    months = {
        '01': 'enero', '02': 'febrero', '03': 'marzo', '04': 'abril',
        '05': 'mayo', '06': 'junio', '07': 'julio', '08': 'agosto',
        '09': 'septiembre', '10': 'octubre', '11': 'noviembre', '12': 'diciembre'
    }
    # Formatear el timestamp
    formatted_timestamp = f"{parts[2]} de {months[parts[1]]} del {parts[0]} a las {parts[3]}:{parts[4]} hs."
    return formatted_timestamp

def prog(texto):
    timestamps = re.findall(r'"timestamp": "T (\d{4}:\d{2}:\d{2} \d{2}:\d{2}:\d{2})",', texto)
    formatted_timestamps = [format_timestamp(ts) for ts in timestamps]
    ret = '\n'.join(formatted_timestamps)
    return ret

if __name__ == '__main__':
    entrada = sys.argv[1]  # archivo entrada (param)
    salida = sys.argv[2]   # archivo salida (param)
    
    f = open(entrada, 'r') # abrir archivo entrada
    datos = f.read()       # leer archivo entrada
    f.close()              # cerrar archivo entrada
    
    ret = prog(datos)      # ejecutar er
    
    f = open(salida, 'w')  # abrir archivo salida
    f.write(ret)           # escribir archivo salida
    f.close()              # cerrar archivo salida
