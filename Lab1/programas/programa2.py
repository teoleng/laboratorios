# -*- coding: utf-8 -*-
import re
import sys

def prog(texto):
    user_counts = {}
    matches = re.findall(r'"user": "(.*?)",', texto)
    for match in matches:
        if match in user_counts:
            user_counts[match] += 1
        else:
            user_counts[match] = 1
    ret = ""
    for i, (user, count) in enumerate(user_counts.items()):
        ret += f"{user}: {count}"
        if i < len(user_counts) - 1:  
            ret += "\n"
    return ret

if __name__ == '__main__':
    entrada = sys.argv[1]  # archivo entrada (param)
    salida = sys.argv[2]   # archivo salida (param)
    
    f = open(entrada, 'r') # abrir archivo entrada
    datos = f.read()       # leer archivo entrada
    f.close()              # cerrar archivo entrada
    
    ret = prog(datos)      # ejecutar er
    
    f = open(salida, 'w')  # abrir archivo salida
    f.write(ret)           # escribir archivo salida
    f.close()              # cerrar archivo salida
