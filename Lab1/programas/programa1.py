# -*- coding: utf-8 -*-
import re
import sys

def prog(texto):
    matches = re.findall(r'@(\w+)', texto)
    unique_users = []
    for match in matches:
        if match not in unique_users:
            unique_users.append(match)
    ret = '\n'.join(unique_users)
    return ret

if __name__ == '__main__':
    entrada = sys.argv[1]  # archivo entrada (param)
    salida = sys.argv[2]   # archivo salida (param)
    
    f = open(entrada, 'r') # abrir archivo entrada
    datos = f.read()       # leer archivo entrada
    f.close()              # cerrar archivo entrada
    
    ret = prog(datos)      # ejecutar er
    
    f = open(salida, 'w')  # abrir archivo salida
    f.write(ret)           # escribir archivo salida
    f.close()              # cerrar archivo salida
